# Full Stack React and Django

This is a 4 hour turorial project by TraversyMedia using Django/React(Redux).

The link to tutorial: [Django/React Tutorial](https://www.youtube.com/watch?v=Uyei2iDA4Hs&list=PLillGF-RfqbbRA-CIUxlxkUpbq0IFkX60)


To run the project:
<br>
    • Install the **requirements**.
<br>
    • Use python to run **makemigrations** and **migrate**.
<br>
    • Run the python server with '**python manage.py runserver**'
<br>
    • Run the webpack with '**npm run dev**'

The project has 4 apps installed called leadmanager, leads, account and frontend.
<br>
This project has a register, login and logout functionality, as well as adding/deleting items in a list.